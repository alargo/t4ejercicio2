
public class TiposVariableApp {

	public static void main(String[] args) {
		int N = 5;
		double A = 4.56;
		char C = 'a';
		double suma = N + A;
		double resta = N - A;
		int valorDeC = (int) C;
		
		System.out.println("Variable N = " + N);
		System.out.println("Variable A = " + A);
		System.out.println("Variable C = " + C);
		System.out.println(N + "+" + A + "=" + suma );
		System.out.println(N + "-" + A + "=" + resta );
		System.out.println("Valor numerico del caracter " + C + " = " + valorDeC);

	}

}
